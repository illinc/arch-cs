#ifdef __APPLE__
#define FUNC_NAME(s) _##s
#elif _WIN64
#define FUNC_NAME(s) s
#elif _WIN32
#define FUNC_NAME(s) _##s
#else
#define FUNC_NAME(s) s
#endif 

.globl FUNC_NAME(main)
FUNC_NAME(main):  
    xor %eax, %eax 
    ret
