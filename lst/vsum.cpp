#include <cstdlib>
#include <climits>
#include <cstdio>
#include <cstddef>

extern "C" ptrdiff_t vsum(double* parray, size_t N, double* psum);

int main() 
{
    const int N = 13;
    double m[N];
    double sum1 = 0;
    for(int i=0; i<N;i++)   
    {
        m[i]= i; 
        printf("%5.2lf ", m[i]);        
        sum1 += m[i];
    }
    puts("");       
    
    printf("parray = %p\n", m );
   
    printf("s1 = %5.2lf\n", sum1 );
    
    
    double sum2 = 0;
    ptrdiff_t len = 0;
    len = vsum(m, N, &sum2);
    //len = vsum(reinterpret_cast<double*>(reinterpret_cast<char*>(m) + 1), N, &sum2);
    printf("s2 = %5.2lf (%d элементов)\n", sum2, len);

    
    return 0;
}
