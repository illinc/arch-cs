.global vsum
// extern "C" ptrdiff_t vsum(double* parray, size_t N, double* psum);
// SysV amd64
// parray ~ rdi
// N ~ rsi
// psum ~ rdx
vsum:
    sub $8, %rsp
    
    mov $-1, %rax
    
    // проверка parray=rdi на выравнивание на 8
    test $(8-1), %rdi   //parray & 7=0b00000111 
    jnz vsum_unaligned_8
    
    
    // верхняя невключаемая граница массива parray + N*sizeof(double)
    lea (%rdi, %rsi, 8), %r10 // r10 = parray + N*sizeof(double)
    
    // округляем parray=rdi вверх до кратного 32
    lea 31(%rdi), %r8 // r8 = parray + 31
    and $-32, %r8     // округляем r8 = parray + 31 до кратного 32 вниз -32=0b1111...11100000
    
    // округляем верхнюю невключаемую границу массива r10 вниз до кратного 32
    mov %r10, %r9
    and $-32, %r9     
    
    // от rdi до r8 и от r9 до r10 — невыравненные double
    // r8 до r9 — выравненные блоки по 32 байта = 4xdouble
    
    
    // Сложение: адрес текущего элемента rcx, сумма xmm0
    xor %rax, %rax    
    vxorpd  %xmm0, %xmm0, %xmm0
    
    mov %rdi, %rcx
    
    // от rdi до r8 ***********************************************************
vsum_unaligned_32_at_start_next:        
    cmp %rcx, %r8 // сравнение (r8-rcx) с 0
    jle vsum_unaligned_32_at_start_complete  // r8-rcx<=0 ⇒ выход из цикла
    
    vaddsd (%rcx), %xmm0, %xmm0   
    inc %rax
    
    add $8, %rcx
    jmp vsum_unaligned_32_at_start_next
    
vsum_unaligned_32_at_start_complete:    

     
    // от r8 до r9 ************************************************************
    
    vxorpd  %ymm1, %ymm1, %ymm1
vsum_aligned_32_next:        
    cmp %rcx, %r9 
    jle vsum_naligned_32_loop_complete  // r9-rcx<=0 ⇒ выход из цикла
    
    vmovapd (%rcx), %ymm2   
    vaddpd %ymm2, %ymm1, %ymm1 //    ymm2[i] + ymm1[i] → ymm1[i]
    add $(32/8), %rax
    
    add $32, %rcx
    jmp vsum_aligned_32_next
vsum_naligned_32_loop_complete: 
    
    vhaddpd %ymm1, %ymm1, %ymm1     // ymm1[0]+ymm1[1] → ymm1[0],  ymm1[2]+ymm1[3] → ymm1[2]
    vpermpd $0b1000, %ymm1, %ymm1   // ymm1[0] → ymm1[0],  ymm1[2] → ymm1[1]
    vhaddpd %xmm1, %xmm1, %xmm1     // xmm1[0]+xmm1[1] → xmm1[0]
    vaddsd %xmm1, %xmm0, %xmm0
    
vsum_naligned_32_all_complete:    


    
    // от r9 до r10 ***********************************************************
vsum_unaligned_32_at_end_next:        
    cmp %rcx, %r10
    jle vsum_unaligned_32_at_end_complete  // r9-rcx<=0 ⇒ выход из цикла
    
    vaddsd (%rcx), %xmm0, %xmm0   
    inc %rax
    
    add $8, %rcx
    jmp vsum_unaligned_32_at_end_next
    
vsum_unaligned_32_at_end_complete:  


    
vsum_end:  
    vmovsd %xmm0, (%rdx)
    

vsum_unaligned_8: 

    add $8, %rsp
    ret
    
