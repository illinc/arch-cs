// Макрос для компенсации искажения имён в Mac OS X (и в MS Windows 32, но в этой программе неактуально)
#ifdef __APPLE__
#define FNNAME(s) _##s
#elif _WIN64
#define FNNAME(s) s
#elif _WIN32
#define FNNAME(s) _##s
#else
#define FNNAME(s) s
#endif 

// System V amd64:  rdi, rsi, rdx, rcx, r8, r9 / xmm0−xmm7 [al]


.globl FNNAME(init1)
.globl FNNAME(init4)
.globl FNNAME(initt)
.globl FNNAME(block_init_4d)

// ****************************************************************************
// extern "C" void init1(char *M, size_t M_len);

.func
FNNAME(init1):     
    // System V amd64: указатель M в %rdi, длина M_len в %rsi (size_t размера указателя)
    
    sub $8, %rsp
    
    mov $'a', %al
    xorl %ecx, %ecx
begin_iteration_1:
    cmp %rsi, %rcx
    jge end_loop_1
    mov %al, (%rdi, %rcx)
    inc %al
    inc %rcx
    jmp begin_iteration_1
end_loop_1:
   
    add $8, %rsp
    ret 
    // выравнивать/восстанавливать %rsp здесь не обязательно, так как нет ни вложенных вызовов, ни xmm-команд — но пусть будет, чтобы плохому не учились
.endfunc

// ****************************************************************************
// extern "C" void init4(int *M, size_t M_len);

.func
FNNAME(init4): 
    sub $8, %rsp
    
    xorl %ecx, %ecx
begin_iteration_4:
    cmp %rsi, %rcx
    jge end_loop_4
    movl $0, (%rdi, %rcx, 4)
    inc %rcx
    jmp begin_iteration_4
end_loop_4:
   
    add $8, %rsp
    ret 
    // выравнивать/восстанавливать %rsp здесь не обязательно — но пусть будет
.endfunc

// ****************************************************************************
// extern "C" void initt(long double *M, size_t M_len, size_t long_double_size);

.func
FNNAME(initt): 
    sub $8, %rsp
    
begin_iteration_T:
    fldpi
    fstpt (%rdi)
    add %rdx, %rdi
    dec %rsi
    jnz begin_iteration_T
   
    add $8, %rsp
    ret 
    // выравнивать/восстанавливать %rsp здесь не обязательно — но пусть будет
.endfunc


// ****************************************************************************
// extern "C" int block_init_4d(double *M, size_t M_len_div_4);

.func
FNNAME(block_init_4d): 
    test $(32-1), %rdi // должно быть 16 штук нулей; маскируем 16-ю единицами
    jnz bad_addr_ymm    

    pushq $0 // +0 double = все нули = 0 quad
    vbroadcastsd (%rsp), %ymm0
    
begin_iteration_ymm:
    vmovapd %ymm0, (%rdi) // vmovApd — di должен быть выравнен на 16
    add $(8*4), %rdi
    dec %rsi
    jnz begin_iteration_ymm
   
    xor %eax, %eax
    add $8, %rsp
    ret 
    // а вот здесь выравнивать/восстанавливать %rsp обязательно
    
bad_addr_ymm:
    mov $-1, %eax
    ret 
  
.endfunc


